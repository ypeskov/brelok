<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov
 * Date: 2019-01-27
 * Time: 23:14
 */

const PATH_TO_LANDS = '/images/lands';

return [
    'Baden-Württemberg' => env('APP_URL').PATH_TO_LANDS.'/baden-wurttemberg.png',
    'Bayern' => env('APP_URL').PATH_TO_LANDS.'/bayern.png',
    'Berlin' =>  env('APP_URL').PATH_TO_LANDS.'/berlin.png',
    'Brandenburg' =>  env('APP_URL').PATH_TO_LANDS.'/brandenburg.png',
    'Bremen' =>  env('APP_URL').PATH_TO_LANDS.'/bremen.png',
    'Deutschland BMI' =>  env('APP_URL').PATH_TO_LANDS.'/deutschland BMI.png',
    'Hamburg' =>  env('APP_URL').PATH_TO_LANDS.'/hamburg.png',
    'Hessen' =>  env('APP_URL').PATH_TO_LANDS.'/hessen.png',
    'Mecklenburg-Vorpommern' =>  env('APP_URL').PATH_TO_LANDS.'/Mecklenburg-Vorpommern.png',
    'Niedersachsen' =>  env('APP_URL').PATH_TO_LANDS.'/Niedersachsen.png',
    'Nordrhein-Westfalen' =>  env('APP_URL').PATH_TO_LANDS.'/Nordrhein-Westfalen.png',
    'Rheinland-Pfalz' =>  env('APP_URL').PATH_TO_LANDS.'/Rheinland-Pfalz.png',
    'Saarland' =>  env('APP_URL').PATH_TO_LANDS.'/Saarland.png',
    'Sachsen' =>  env('APP_URL').PATH_TO_LANDS.'/Sachsen.png',
    'Sachsen-Anhalt' =>  env('APP_URL').PATH_TO_LANDS.'/Sachsen-Anhalt.png',
    'Schleswig-Holstein' =>  env('APP_URL').PATH_TO_LANDS.'/Schleswig-Holstein.png',
    'Thüringen' =>  env('APP_URL').PATH_TO_LANDS.'/Thüringen.png',
];