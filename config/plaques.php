<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 2019-01-28
 * Time: 13:04
 */

const PATH_TO_PLAQUES = '/images/plaques';

return [
    'Rosa' => env('APP_URL').PATH_TO_PLAQUES.'/rose.png',
    'Grün' => env('APP_URL').PATH_TO_PLAQUES.'/green.png',
    'Orange' => env('APP_URL').PATH_TO_PLAQUES.'/orange.png',
    'Blau' => env('APP_URL').PATH_TO_PLAQUES.'/blue.png',
    'Gelb' => env('APP_URL').PATH_TO_PLAQUES.'/yellow.png',
    'Braun' => env('APP_URL').PATH_TO_PLAQUES.'/brown.png',
];