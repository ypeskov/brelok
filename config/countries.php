<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov
 * Date: 2019-01-27
 * Time: 22:26
 */

const PATH_TO_COUNTRIES = '/images/countries';

return [
    'germany' => env('APP_URL').PATH_TO_COUNTRIES.'/germany-icon.png',
    'austria' => env('APP_URL').PATH_TO_COUNTRIES.'/austria-icon.png',
];