<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/preview-pdf', 'GeneratePdfController@previewPdf')->name('preview-pdf');
Route::get('/generate-pdf', 'GeneratePdfController@generatePdf')->name('generate-pdf');

Route::post('/upload-file', 'UploadFileController@upload')->name('upload-file');