<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    public function upload(Request $request)
    {
        if ($request->file('image')->isValid()) {

            $request
                ->file('image')
                ->storeAs('images', $request->file('image')->getClientOriginalName());

            return redirect()
                ->route('home')
                ->with(['upload_success' => __('app.upload success')]);
        } else {
            return redirect()
                ->route('home')
                ->with(['upload_error' => __('app.upload error')]);
        }
    }
}
