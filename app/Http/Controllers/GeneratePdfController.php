<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use mikehaertl\wkhtmlto\Pdf;

class GeneratePdfController extends Controller
{
    protected $currentDomain;

    public function __construct()
    {
        $this->currentDomain = env('APP_URL', 'http://brelok.test');
    }

    public function previewPdf(Request $request)
    {
        $params = $request->all()['brelok_list'];
        $brelokList = json_decode($params);

        return view('pages.preview', [
            'brelokList' => $brelokList,
            'isPreview' => true,
            'params' => "brelok_list=$params",
            'domain' => $this->currentDomain,
        ]);
    }

    public function generatePdf(Request $request)
    {
        $brelokList = json_decode($request->all()['brelok_list']);

        $pdf  = new Pdf();

        $html = \View::make('pages.preview', [
            'brelokList' => $brelokList,
            'isPreview' => false,
            'domain' => $this->currentDomain,
        ])
            ->render();

//        $pdf->addPage($html)->send('breloks.pdf');
        $pdf->addPage($html)->send();
    }
}
