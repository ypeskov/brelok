<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        $flags = $this->getFilesByType('images/flags');
        $autoBrands = $this->getFilesByType('images/autoBrands');
        $otherImages = $this->getFilesByType('images/otherImages');
        $availableFonts = $this->getFilesByType('fonts');

        $contentTypes = [
            'flags' => __('app.flag'),
            'autoBrands' => __('app.auto brand'),
            'otherImages' => __('app.other image'),
            'number' => __('app.number'),
            'text' => __('app.text'),
        ];

        return view('pages.home', [
            'contentTypes' => json_encode($contentTypes),
            'flags' => json_encode($flags),
            'autoBrandImages' => json_encode($autoBrands),
            'otherImages' => json_encode($otherImages),
            'availableFonts' => json_encode($availableFonts),
            'countries' => json_encode(config('countries')),
            'lands' => json_encode(config('lands')),
            'plaques' => json_encode(config('plaques')),
        ]);
    }

    /**
     * @param string $type
     * @return array
     */
    protected function getFilesByType(string $type)
    {
        $files = scandir(public_path()."/$type");

        $imageFiles = [];
        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                $imageFiles[] = $file;
            }
        }

        return $imageFiles;
    }
}
