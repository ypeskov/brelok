<?php

return [
    'design brelok' => 'design brelok',
    'Brelok' => 'Brelok',
    'select country' => 'Select the country',
    'flag' => 'flag',
    'auto brand' => 'automobile brand',
    'text' => 'text',
    'other image' => 'other image',
    'upload file' => 'Upload File',
    'upload success' => 'Upload was successful',
    'upload error' => 'An error happened during file upload',
    'upload info' => 'Select image to upload:',
    'number' => 'number',
];