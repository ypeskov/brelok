@font-face {
    font-family: 'EuroPlate';
    src: url('{{env('PATH_TO_FONTS')}}/EuroPlate.ttf') format('truetype');
}

@font-face {
    font-family: 'Homoarak';
    src: url('{{env('PATH_TO_FONTS')}}/HOMOARAK.ttf') format('truetype');
}

@font-face {
    font-family: 'BadaboomBB_Reg';
    src: url('{{env('PATH_TO_FONTS')}}/BadaboomBB_Reg.ttf') format('truetype');
}

@font-face {
    font-family: 'CANDY___';
    src: url('{{env('PATH_TO_FONTS')}}/CANDY___.ttf') format('truetype');
}

@font-face {
    font-family: 'CATHSGBR';
    src: url('{{env('PATH_TO_FONTS')}}/CATHSGBR.ttf') format('truetype');
}

@font-face {
    font-family: 'PORKH___';
    src: url('{{env('PATH_TO_FONTS')}}/PORKH___.ttf') format('truetype');
}

@font-face {
    font-family: 'SaucerBB';
    src: url('{{env('PATH_TO_FONTS')}}/SaucerBB.ttf') format('truetype');
}

@font-face {
font-family: 'Super Webcomic Bros Bold Italic';
src: url('{{env('PATH_TO_FONTS')}}/Super Webcomic Bros Bold Italic.ttf') format('truetype');
}

@font-face {
    font-family: 'LatoLatin-Black';
    src: url('{{env('PATH_TO_FONTS')}}/LatoLatin-Black.ttf') format('truetype');
}

@font-face {
    font-family: 'Super Webcomic Bros Bold';
    src: url('{{env('PATH_TO_FONTS')}}/Super Webcomic Bros. Bold.ttf') format('truetype');
}

@font-face {
    font-family: 'Super Webcomic Bros';
    src: url('{{env('PATH_TO_FONTS')}}/Super Webcomic Bros.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBA';
    src: url('{{env('PATH_TO_FONTS')}}/ALBA.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBAM';
    src: url('{{env('PATH_TO_FONTS')}}/ALBAM.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBAS';
    src: url('{{env('PATH_TO_FONTS')}}/ALBAS.ttf') format('truetype');
}