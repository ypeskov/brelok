@font-face {
    font-family: 'EuroPlate';
    src: url('/fonts/EuroPlate.ttf') format('truetype');
}

@font-face {
    font-family: 'Homoarak';
    src: url('/fonts/HOMOARAK.ttf') format('truetype');
}

@font-face {
    font-family: 'BadaboomBB_Reg';
    src: url('/fonts/BadaboomBB_Reg.ttf') format('truetype');
}

@font-face {
    font-family: 'CANDY___';
    src: url('/fonts/CANDY___.ttf') format('truetype');
}

@font-face {
    font-family: 'CATHSGBR';
    src: url('/fonts/CATHSGBR.ttf') format('truetype');
}

@font-face {
    font-family: 'PORKH___';
    src: url('/fonts/PORKH___.ttf') format('truetype');
}

@font-face {
    font-family: 'SaucerBB';
    src: url('/fonts/SaucerBB.ttf') format('truetype');
}

@font-face {
    font-family: 'Super Webcomic Bros Bold Italic';
    src: url('/fonts/Super Webcomic Bros Bold Italic.ttf') format('truetype');
}

@font-face {
    font-family: 'Super Webcomic Bros Bold';
    src: url('/fonts/Super Webcomic Bros. Bold.ttf') format('truetype');
}

@font-face {
    font-family: 'Super Webcomic Bros';
    src: url('/fonts/Super Webcomic Bros.ttf') format('truetype');
}

@font-face {
    font-family: 'LatoLatin-Black';
    src: url('/fonts/LatoLatin-Black.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBA';
    src: url('/fonts/ALBA.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBAM';
    src: url('/fonts/ALBAM.ttf') format('truetype');
}

@font-face {
    font-family: 'ALBAS';
    src: url('/fonts/ALBAS.ttf') format('truetype');
}