<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{__('app.design brelok')}}</title>
    <link href="/css/app.css" rel="stylesheet" />
</head>

<body>
<div id="app" class="container">
    <div class="row">
        <div class="col-lg-12">
            <header>
                <h1 style="font-family: EuroPlate; font-size: 10mm;">{{__('app.Brelok')}}</h1>
            </header>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-12">
            @if ($message = Session::get('upload_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>

        @if ($message = Session::get('upload_error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-lg-12">
            @yield('content')
        </div>
    </div>
</div>

<script src="/js/app.js"></script>
</body>
</html>