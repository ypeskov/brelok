<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">

    <style>
        .preview-block {
            width: 210mm;
            height: 297mm;
            border: 1px solid black;
            padding: 1em;
        }

        .brelok-side, .image-content {
            width: 51.58mm;
            height: 10.8mm;
            border: 1px dashed black;
            line-height: 10.8mm;
        }

        .generate-pdf-button {
            background-color: #6c757d;
            margin: 1em 0;
            font-size: 2em;
        }

        .preview-url > button {
            font-size: 2em;
            font-weight: bold;
        }

        .front-side, .back-side {
            float: left;
            margin-right: 10mm;
        }

        .separator {
            height: 15mm;
        }

        .number-preview {
            width: 51.58mm;
            height: 10.8mm;
            border: 1px dashed #1b1e21;
            display: inline-flex;
            background-color: white;
            font-family: EuroPlate;
            text-align: left;
            font-size: 7mm;
            position: relative;
            margin-right: 10mm;
            float: left;
        }

        .country-img {
            height: 10.8mm !important;
            width: 5mm !important;
        }

        .land-img, .plaque-img {
            width: 5mm;
            height: 5mm;
            left: 17mm;
        }

        .plaque-img {
            position: absolute;
            top: 0.5mm;
        }

        .land-img {
            position: absolute;
            top: 5.5mm;
        }

        .land-img-container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            margin: 0.1em 0 0.1em 0.2em;
        }

        .group1, .group2, .group3 {
            font-family: EuroPlate;
            font-size:7mm;
            top: 2mm;
            position: absolute;
        }

        .group1 {
            left: 7mm;
        }

        .group2 {
            left: 23mm;
        }

        .group3 {
            left: 33mm;
        }

        @if ($isPreview)
            @include('layouts.preview-fonts')
        @else
            @include('layouts.pdf-fonts')
        @endif
    </style>
</head>

<body>@yield('content')</body>

</html>