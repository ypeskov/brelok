@extends('layouts.pdf')

@section('content')

    @if ($isPreview)
        <div>
            <a href="/generate-pdf?{{$params}}">
                <button class="generate-pdf-button">Generate PDF</button>
            </a>
        </div>
    @endif

    <div class="preview-block">
        @foreach($brelokList as $brelok)
            <div class="brelok-row">
                @if (isset($brelok->dataFront))
                    @if (isset($brelok->dataFront->type) && $brelok->dataFront->type == 'text')
                        <div class="brelok-side front-side" style="{{$brelok->dataFront->style}}">
                            {{$brelok->dataFront->text}}
                        </div>
                    @elseif (isset($brelok->dataFront->type) && $brelok->dataFront->type == 'number')
                        <div class="number-preview">
                            @if ($brelok->dataFront->data && $brelok->dataFront->data->country)
                                <span class="country-img-container">
                                    <img class="country-img" src="{{$brelok->dataFront->data->country->url}}" />
                                </span>
                            @endif

                            <span class="group1">{{$brelok->dataFront->data->group1}}</span>

                            <span class="land-img-container">
                                @if ($brelok->dataFront->data && strlen($brelok->dataFront->data->plaque->url) > 0)
                                    <img class="plaque-img" src="{{$brelok->dataFront->data->plaque->url}}" />
                                @endif

                                @if ($brelok->dataFront->data && strlen($brelok->dataFront->data->land->url) > 0)
                                    <img class="land-img" src="{{$brelok->dataFront->data->land->url}}" />
                                @endif
                            </span>

                            <span class="group2">{{$brelok->dataFront->data->group2}}</span>

                            <span class="group3">{{$brelok->dataFront->data->group3}}</span>
                        </div>
                    @else
                        <div class="brelok-side front-side">
                            <img class="image-content"
                                 src="{{$domain}}{{$brelok->dataFront->image}}" alt="{{$brelok->dataFront->image}}">
                        </div>
                    @endif
                @endif

                @if (isset($brelok->dataBack))
                    @if (isset($brelok->dataBack->type) && $brelok->dataBack->type == 'text')
                        <div class="brelok-side back-side" style="{{$brelok->dataBack->style}}">
                            {{$brelok->dataBack->text}}
                        </div>
                    @elseif (isset($brelok->dataBack->type) && $brelok->dataBack->type == 'number')
                        <div class="number-preview">
                            @if ($brelok->dataBack->data && $brelok->dataBack->data->country)
                                <span class="country-img-container">
                                    <img class="country-img" src="{{$brelok->dataBack->data->country->url}}" />
                                </span>
                            @endif

                            <span class="group1">{{$brelok->dataBack->data->group1}}</span>

                            <span class="land-img-container">
                                @if ($brelok->dataBack->data && strlen($brelok->dataBack->data->plaque->url) > 0)
                                    <img class="plaque-img" src="{{$brelok->dataBack->data->plaque->url}}" />
                                @endif

                                @if ($brelok->dataBack->data && strlen($brelok->dataBack->data->land->url) > 0)
                                    <img class="land-img" src="{{$brelok->dataBack->data->land->url}}" />
                                @endif
                            </span>

                            <span class="group2">{{$brelok->dataBack->data->group2}}</span>

                            <span class="group3">{{$brelok->dataBack->data->group3}}</span>
                        </div>
                    @elseif (isset($brelok->dataBack->image))
                        <div class="brelok-side back-side">
                            <img class="image-content"
                                 src="{{$domain}}{{$brelok->dataBack->image}}" alt="{{$brelok->dataBack->image}}">
                        </div>
                    @endif
                @endif
            </div>

            <div class="separator"></div>

        @endforeach
    </div>

@endsection