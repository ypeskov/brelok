@extends('layouts.main')

@section('content')

    <form action="/upload-file" method="POST" enctype="multipart/form-data">
        @csrf
        <label for="image_upload">{{__('app.upload info')}}</label>
        <input id="image_upload" type="file" name="image" />
        <input type="submit" value="{{__('app.upload file')}}"/>
    </form>

    <hr>

    <brelok-list
        initial-flag-names="{{$flags}}"
        initial-auto-brand-images="{{$autoBrandImages}}"
        initial-other-images="{{$otherImages}}"
        initial-types="{{$contentTypes}}"
        initial-countries="{{$countries}}"
        initial-lands="{{$lands}}"
        initial-plaques="{{$plaques}}"
        initial-fonts="{{$availableFonts}}">
    </brelok-list>
@endsection